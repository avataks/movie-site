import styled from "styled-components";

export const Wrapper = styled.section`
  width: 100%;
  background: #f67f45;
  padding: 30px 50px;
  min-height: calc(100vh - 70px);
`;
