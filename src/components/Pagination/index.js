import React from 'react'
import "./styled.css"
import { Link } from 'react-router-dom'

export const Pagination = ({ page_paginat, totalPages_paginat }) => {
    return (
        <div className="navigation">{page_paginat !== "1" ? <Link className="Link_navi" to={`/main/${+page_paginat - 1}`}>«</Link> : null}
            {+page_paginat > "2" ? <Link className="Link_navi" to={`/main/${+page_paginat - (+page_paginat - 1)}`}>1</Link> : null}
            {page_paginat > 3 ? <span className="Link_navi_span" >...</span> : null}
            {page_paginat !== "1" ? <Link className="Link_navi" to={`/main/${+page_paginat - 1}`}>{+page_paginat - 1}</Link> : null}
            {<Link className="Link_navi_orig" to={`/main/${page_paginat}`}>{page_paginat}</Link>}
            {(+page_paginat + 1) >= totalPages_paginat ? null : <Link className="Link_navi" to={`/main/${+page_paginat + 1}`}>{+page_paginat + 1}</Link>}
            {(+page_paginat + 2) < totalPages_paginat ? <span className="Link_navi_span" >...</span> : null}
            {+page_paginat < totalPages_paginat ? <Link className="Link_navi" to={`/main/${totalPages_paginat}`}>{totalPages_paginat}</Link> : null}
            {+page_paginat < totalPages_paginat ? <Link className="Link_navi" to={`/main/${+page_paginat + 1}`}>»</Link> : null}</div>
    )
};