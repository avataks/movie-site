import React from 'react'
import "./style.css"

window.onload = function () {
    document.body.classList.add('loaded_hiding');
    window.setTimeout(function () {
        document.body.classList.add('loaded');
        document.body.classList.remove('loaded_hiding');
    }, 500);
}

export const Loading = () => {
    return (
        <div class="preloader">
            <div class="preloader__image"></div>
        </div>
    )
}