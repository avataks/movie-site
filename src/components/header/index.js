import React from "react";
import { Link } from "react-router-dom";

import { Header as HeaderStyled } from "./styled";

export const Header = () => {
  return (
    <HeaderStyled>
      <div>
        <Link to="/main/1" className="nav_link">Main</Link>
        <Link to="/favorites" className="nav_link">Favorites</Link>
      </div>
    </HeaderStyled>
  );
};
