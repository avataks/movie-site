import styled from "styled-components";

export const Header = styled.header`
  background: #292c33;
  height: 70px;
  width: 100%;
  padding: 0 95px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  z-index:1;
  a {
    color: #fff;
    margin-right: 45px;
    text-decoration: none;
  }
  a:hover{
    color:#f67f45;
  }
  a:hover::before{
    content:"";
    width: calc(100% + 40px);
    height: 1px;
    background:#f67f45;
    position:relative;
    display:block;
    left: -18px;
    top: 39px;
  }
  >div{
    position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  }
`;
