import React, { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux'
import { setFavoritesFull } from '../../store/actions'
import Api from '../../helpers/api'
import { Card, Flex, IconBlock } from "../main/styled"
import { toggleFavorites } from "../../store/actions";
import { ReactComponent as StarIcon } from '../../assets/star.svg'
import { Link } from 'react-router-dom'
import moment from 'moment'
import "../main/styled_main.css"

export const Favorites = () => {
  const favoritesFull = useSelector(store => store.favoritesFull);
  const favorites = useSelector(store => store.favorites);
  const dispatch = useDispatch();

  useEffect(() => {
    const getData = async () => {
      if (favorites.length && favorites.length !== favoritesFull.length) {
        Promise.all(favorites.map((id) => Api.getDetails(id))).then((res) => {
          dispatch(setFavoritesFull(res))
        });
      }
    }
    getData();
  }, [])

  const renderMovies = () => {
    return favoritesFull.map(({ title, id, poster_path, release_date, vote_average }) => (
      <Card key={`movies-${id}`}>
        <Link className="tooltip" to={`/details/${id}`}><span class="tooltiptext">Details...</span><img src={Api.poster_url + poster_path} className="poster_link" /></Link>
        <IconBlock onClick={() => dispatch(toggleFavorites(id))} active={favorites.includes(id)}>
          <StarIcon /></IconBlock>
        <div className="reiting" ><div className="average">{vote_average}</div></div>
        <h3>{title.length >= 45 ? title.slice(0, 45) + "..." : title + " " + (moment(release_date).format('(YYYY)'))}</h3>
      </Card>
    ));
  }

  return (
    <section>
      <Flex>{renderMovies()}</Flex>
    </section>
  )
};
