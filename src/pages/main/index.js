import React, { useEffect } from 'react'
import Api from "../../helpers/api"
import { useDispatch, useSelector } from "react-redux"
import { Link } from 'react-router-dom'
import { setPage, toggleFavorites } from "../../store/actions";
import { Card, Flex, IconBlock } from "./styled"
import moment from 'moment'
import { ReactComponent as StarIcon } from '../../assets/star.svg'
import "./styled_main.css"
import { Loading } from "../../components/Loading/index"
import { Pagination } from "../../components/Pagination/index"

export const MainPage = ({
  match: {
    params: { page },
  },
}) => {

  const movies = useSelector(store => store.movies);
  const totalPages = useSelector(store => store.totalPages);
  const favorites = useSelector(state => state.favorites);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!movies[page]) {
      Api.getNowPlayng(page).then((data) => {
        dispatch(setPage(page, data.results));
      });
    }
  }, [page]);

  useEffect(() => {
    console.log(favorites)
  }, [favorites])

  const renderMovies = () => {
    return movies[page].map(({ title, id, poster_path, release_date, vote_average }) => (
      <Card key={`movies-${id}`}>
        <Link className="tooltip" to={`/details/${id}`}><span class="tooltiptext">Details...</span><img src={Api.poster_url + poster_path} className="poster_link" /></Link>
        <IconBlock onClick={() => dispatch(toggleFavorites(id))} active={favorites.includes(id)}>
          <StarIcon /></IconBlock>
        <div className="reiting" ><div className="average">{vote_average}</div></div>
        <h3>{title.length >= 45 ? title.slice(0, 45) + "..." : title + " " + (moment(release_date).format('(YYYY)'))}</h3>
      </Card>
    ));
  }

  return <section>
    <Pagination page_paginat={page} totalPages_paginat={totalPages} />
    <Flex>{movies[page] ? renderMovies() : <Loading />}</Flex>
  </section>
};

