import React, { useEffect } from 'react'
import Api from "../../helpers/api"
import moment from 'moment'
import { useDispatch, useSelector } from "react-redux"
import { setMovieDetails } from '../../store/actions'
import "./styled_teg.css"
import { Img_back } from "./styled"
import { Loading } from "../../components/Loading/index"

export const MovieDetails = ({
    match: {
        params: { id },
    },
}) => {
    const moviesDetails = useSelector((store) => store.moviesDetails);
    const genresList = useSelector((store) => store.genres);
    const dispatch = useDispatch();
    useEffect(() => {
        if (!moviesDetails[id]) {
            Api.getDetails(id).then((data) => {
                dispatch(setMovieDetails(id, data))
            })
        }
    }, [id]);

    const renderGenres = (genres) => {
        let str = `Genres: `;
        genres.forEach(({ name }, i) => {
            str += `${name}${i < genres.length - 1 ? ", " : "."}`
        });

        return str;
    }
    const renderCountries = (production_countries) => {
        let str = `Countries: `;
        production_countries.forEach(({ name }, i) => {
            str += `${name}${i < production_countries.length - 1 ? ", " : "."}`
        });

        return str;
    }

    if (moviesDetails[id]) {
        const { genres, title, poster_path, tagline, overview, backdrop_path, release_date, vote_average, production_countries, runtime } = moviesDetails[id];
        return (<section>
            <Img_back bg={Api.poster_url + backdrop_path}></Img_back>
            <div className="liniya"></div>
            <img className="poster" src={Api.poster_url + poster_path} />
            <div className="position">
                <h2>{title + " " + (moment(release_date).format('(YYYY)'))}</h2>
                <p>{renderGenres(genres)}</p>
                <p>{moment(release_date).format('DD-MM-YYYY')}</p>
                <p>{renderCountries(production_countries)}</p>
                <p>Diration: {runtime > 120 ? parseInt(runtime / 60) + "." + (runtime - 120) : parseInt(runtime / 60) + "." + (runtime - 60)} h</p>
                <p>Rating: {vote_average}</p>
                <p className="tagline">{tagline}</p>
                <p>{overview}</p>
            </div>
        </section>
        );
    } else {
        return <Loading />;
    }
};