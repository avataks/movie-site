import styled from "styled-components";

export const Img_back = styled.section`
background:url(${(props) => props.bg});
width: 109%;
height: 127%;
left: -79px;
top: -34px;
position: fixed;
background-size:cover;
filter:blur(4px);
border: 4px solid black;
overflow:hidden;
`;
